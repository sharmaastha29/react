/* Here, we will see how to bind fxn to a component in react js. 
Instead of putting .bind(this) everytym for every fxn what we can do 
is we can create a separate fxn  which will bind this and we simply 
will import that component in which that fxn is there and call that 
fxn and pass the fxn name(s). For this, we would create a separate 
util.js file and use a utility mtd inside that to write that fxn to bind.
*/
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TODOItem from './components/TODOItem.js'; 
import TODOForm from './components/TODOForm.js';
import bindFunctions from './util.js'; // importing the js file which has fxn to avoid repetitive binding fxnlty. Only one . to reach that file as index is already in src but TODOItem is inside src inside components.


class TODOList extends React.Component{

constructor(){
super();
// this.changeStatus=this.changeStatus.bind(this);
// this.updateTask=this.updateTask.bind(this);
// this.addTask=this.addTask.bind(this);
// this.deleteTask=this.deleteTask.bind(this);
// this.editItem=this.editItem.bind(this);
bindFunctions.call(this,['changeStatus','updateTask','addTask','deleteTask','editItem']) //In this bindFunctions just pass this and all fxns one by one which you need to bind with 'this'.
//We are not calling like: bindFunctions(); bcs it will change the value of this and we want the value of this to point to our class component. Similarly, do for TODOItem.js
// Also, if we add a new fxn we can simply add the name of that new fxn in the array ['changeStatus','updateTask','addTask','deleteTask','editItem']
this.state={
tasks:[
    {name:"Buy Milk",
    completed:false},

    {name:"Buy Cheese",
    completed: false},

    {name:"Buy Bread",
     completed: false}
      ],
currentTask: ' '      
}
}



editItem(index, newVal){
console.log(index, newVal);
let tsk=this.state.tasks;
let t=tsk[index]; 
t['name']=newVal;
this.setState({
    tasks:tsk
})
}





deleteTask(index){ 
let tsk=this.state.tasks;
tsk.splice(index, 1);
this.setState({     
tasks:tsk
})
}


addTask(evt){
evt.preventDefault();
let tsk=this.state.tasks; 
let currTask=this.state.currentTask; 
tsk.push({ 
    name:currTask, 
    completed:false 
})
this.setState({
tasks:tsk,
currentTask: ' ' 
})
}


updateTask(newValue){
this.setState({currentTask:newValue.target.value})
}


changeStatus(idx){
console.log(this.state.tasks[idx]) ;
var tsk=  this.state.tasks; 
var t=tsk[idx]; 
t.completed=!t.completed; 
this.setState({tasks:tsk}) 
}


render(){ 
    return( <section> 
            <TODOForm  detail1={this.state.currentTask} clickUpdater={this.updateTask} clickSubmitter={this.addTask} />
            <ul>{this.state.tasks.map((task,idx)=>{
            return <TODOItem clickHandler={this.changeStatus}  clickDeleter={this.deleteTask} index={idx}key={/*task.name*/idx} detail={task} clickediter={this.editItem}/>
   
   })}
    </ul>
    </section>)
}
}




ReactDOM.render(<TODOList/>, document.getElementById('root'))
