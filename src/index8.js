/*Here we will do see in detail about ComponentDidMount() lifecycle mtd of react. It can be used to send ajax requests.How to send that we will see.
We need to mk sure that there is a jquery installed in the package.json of your project as jquery is needed for ajax requests. Also, import jquery in index.js You can verify jquery installation by clicking in package json in left side menu and under dependencies section you will find jquery with its version. It means jquery is installed.
About ComponentDidMount() mtd-->>>it is called after render mtd and is appropriate for making ajax, axios, fetch  etc calls from inside this. */

import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery'; //By writing $ symbol we can use jquery library as $ symbol during ajax calls and other places as well.
import   './index.css';

class App extends React.Component{

    constructor(props){
        super(props);
   this.state={ //declaring state in order to store the data coming in response(json) of the ajax request call.
       users:[] // all the json data coming from ajax call will be stored in users array. We will setState of data in users array.
   }

    }
    render(){
        return( //to display name field coming from json response we will write javascript inside list.
         <ul>
              {this.state.users.map((usr)=>{
                  return (<li key={usr.id}> {usr.name} </li>)
              })}


         </ul>
        )}



        componentDidMount() //It is called after render () mtd but we can write above render() also.
        {
        $.ajax (// It is a mtd inside it we will write request-url, success/error mtds which will hold corresponding response data.
            {
              url:'https://jsonplaceholder.typicode.com/users',
              success:(data)=>{ // using fat arrow mtd for success bcs value of 'this' keyword changes and thus we can't access  above state from inside success mtd if we don't use fat arrow mtd . Whatever data comes in result/response will be available in success.
               this.setState({
                   users:data //all json data coming will be stored and saved in users array by writing in setState() mtd
               })
              }
        
            }
        )
        }

}
// JSX is always written within{}
//Remember Internet connection is a must else on hitting url in ajax call it won't be able to bring result in response and so data will not be able to be viewed on browser screen.



ReactDOM.render(<App/>,document.getElementById('root'))