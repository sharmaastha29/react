module.exports=function bindFunctions (functions){

    functions.forEach(f=>(this[f]=this[f].bind(this)));
};

/*This abv code helps in prevent writing fxn bind again n again.*/