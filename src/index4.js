import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TODOItem from './components/TODOItem7.js/index.js'; //for all custom components inside components folder we need to give export there and import in index.js page 
import TODOForm from './components/TODOForm.js';


class TODOList extends React.Component{

constructor(){
super();
this.changeStatus=this.changeStatus.bind(this);
this.updateTask=this.updateTask.bind(this);
this.addTask=this.addTask.bind(this);
this.deleteTask=this.deleteTask.bind(this);
this.editItem=this.editItem.bind(this);
this.state={
tasks:[
    {name:"Buy Milk",
    completed:false},

    {name:"Buy Cheese",
    completed: false},

    {name:"Buy Bread",
     completed: false}
      ],
currentTask: ' '      
}
}

editItem(index, newVal){ //This fxn adds the new data to tasks array when someone clicks edit button and modifies data in textbox and clicks on update button.
// This edit Item will take two things- index, new Value so depending on index and newValue it will update the list.With help of index it will get to know exactly which element in list is changing and there it will add new value.
console.log(index, newVal);
/*Now add this index and value coming from textbox into taks array so that it could be added to the list and displayed when all the other list items are displayed */
let tsk=this.state.tasks;
let t=tsk[index]; //by giving index of whichevr list item we are updating it will assign that particular record in t.
t['name']=newVal;//name is changed from old name to the new item's name and then we need to save it so we need to do setState.
this.setState({
    tasks:tsk
})
/*Now it is set to tasks state now we need to display it as list. for that we need to call toggle() inside updateItem() in TODOItem.js*/
}





deleteTask(index){ // This fxn deletes the item at given index which is pressed from list 
/* This function deltes the added items in the list. Delete button will be there infront of every
individual list item. So it should be put in TODOItem component  as that is where list tag is used
and it is listing all data items one by one. Now, how it will get to know which item is being 
clicked so for that we will use index concept of tasks array which is already there in TODOItem 
component. Here, in index page we need to pass this deleteTask() to the child component ->TODOItem*/
console.log(index); //when delete button is pressed it will capture in logs which indexed delete button was pressed from all in the list.
/*Using splice function we can remove data from array . splice() will take 2 things-> (index from where value is to be removed, limit till which value needs to be removed)*/
let tsk=this.state.tasks;// creating clone/ copy of tasks state
tsk.splice(index, 1);
this.setState({ // to save and reflect the data in state
tasks:tsk
})
}


addTask(evt){// This method adds current data to list and displays it as list and resets the current data value.
/* This function will push/submit whatever is added in textbox to the state tasks bcs they are being
 displayed on screen in form of list so basically it tells how to add textbox value to list by 
 clicking on submit button ; the function addTask will accept the event coming from screen when user clicks the submit button*/
evt.preventDefault();// This method will prevent the page from refreshing again and again. Otherwise, after adding the value it will refresh the page
let tsk=this.state.tasks; //creating copy of tasks state and storing it in a var --> recommended way before making any changes to that var
let currTask=this.state.currentTask; //creating copy of state currentTask and storing it in variable--> recommended way before making any changes to that var
tsk.push({ // pushing newly added current value to the task state so that it can be seen as list like rest of the tasks; obviously it will be pushed as key value pair just in the same format as is accepted by tasks state
    name:currTask, //adding currTask variable which has actual current value as name field in Tasks
    completed:false 
})
this.setState({// making the changes permanent by putting it inside setState -> assigning task 
tasks:tsk,
currentTask: ' ' //To reset the textbox after value from textbox is added to the state and displayed in the list on screen
})
}

updateTask(newValue){//updateTask mtd will modify currentTask state
this.setState({currentTask:newValue.target.value})//this.setState() will save that permanently. Also, as newvalue is an event so we need to extract actual value from it by writing->newValue.target.value 
}

changeStatus(idx){
console.log(this.state.tasks[idx]) ;
var tsk=  this.state.tasks; //stores the entirre array in one var for easiness; it is a recommended technique to take clone of the existing state before changing it.
var t=tsk[idx]; //takes one object entry and stores in var t
t.completed=!t.completed; //changes t.completed true to t.completed false and vice versa
this.setState({tasks:tsk}) //this.setstate() is an inbuilt mtd which sets the state permanently. Here, we are setting main tasks array with the changed tsk. If you want any changes to reflect permanently then you would need to do setState and in setstate key represents state and value represents the one which is there in the function which we want to assign to key. If both are named same then we can directly pass one and no need to make it in key:value form
}

render(){ 
    return( <section> 
            <TODOForm  detail1={this.state.currentTask}/*currentTask is in state so we will call it using this.state.currentTask */ clickUpdater={this.updateTask} clickSubmitter={this.addTask} />
            <ul>{this.state.tasks.map((task,idx)=>{
            return <TODOItem clickHandler={this.changeStatus}  clickDeleter={this.deleteTask} index={idx}key={/*task.name*/ task.idx} detail={task} clickediter={this.editItem}/>
   // Here, in index.js we have added key in map as task.name but someone can even add item with same name so it is not recommended to make tak.name as key bcs it will otherwise not let you enter item with same name so instead we can make key as task.index.Now it will not complain of unique child keys error
   })}
    </ul>
    </section>)
}
}

// class TODOItem extends React.Component{

//     render(){
 
//           return (<li onClick={()=>{this.props.clickHandler(this.props.index);} } className={this.props.detail.completed ?'complete':''}>{ this.props.detail.name}</li>)
//           /*className={this.props.detail.completed ?'complete':''} is for setting css properties */
//         }
// }

ReactDOM.render(<TODOList/>, document.getElementById('root'))


/*In this part, we will first organize our application. We will create our main component which is
 to be displayed in react DOM in index.js page. Rest all components we will place in a folder under 
 src --> src/Component/your component.js file. We will then use export keyword to make that 
 component folder file accessible globally. Also, we have to add common imports specific to 
 component class like: React etc.. Here, see in src folder there we have created one more folder 
 called-> component (to keep all the components). Inside this component we have kept our TOList 
 component. Also we need to import TOdoItem component inside TodoList component.For all custom 
 components which we create inside components folder we need to import giving a relative path 
 inside index.js */

 /* Install React dev tools extension on chrome- https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en*/ 

 /*var and let are both used for function declaration in javascript but the difference between them 
 is that var is function scoped and let is block scoped. It can be said that a variable declared 
 with var is defined throughout the program as compared to let. var variables can be accessed globally */