import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


class TODOList extends React.Component{

constructor(){
super();
this.changeStatus=this.changeStatus.bind(this);
this.state={
tasks:[
    {name:"Buy Milk",
    completed:false},

    {name:"Buy Cheese",
    completed: false},

    {name:"Buy Bread",
     completed: false}
]
}
}

changeStatus(idx){
console.log(this.state.tasks[idx]) ;
var tsk=  this.state.tasks; //stores the entirre array in one var for easiness; it is a recommended technique to take clone of the existing state before changing it.
var t=tsk[idx]; //takes one object entry and stores in var t
t.completed=!t.completed; //changes t.completed true to t.completed false and vice versa
this.setState({tasks:tsk}) //this.setstate() is an inbuilt mtd which sets the state permanently. Here, we are setting main tasks array with the changed tsk 
}

render(){
    return( <ul>{this.state.tasks.map((task,idx)=>{
        return <TODOItem clickHandler={this.changeStatus} index={idx}key={task.name} detail={task}/>
    })}
    </ul>)
}
}

class TODOItem extends React.Component{

    render(){
 
          return (<li onClick={()=>{this.props.clickHandler(this.props.index);} } className={this.props.detail.completed ?'complete':''}>{ this.props.detail.name}</li>)
          /*className={this.props.detail.completed ?'complete':''} is for setting css properties */
        }
}

ReactDOM.render(<TODOList/>, document.getElementById('root'))