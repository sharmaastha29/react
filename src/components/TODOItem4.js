import React from 'react';

class TODOItem extends React.Component{
    constructor(props){ 
        super(props);
    this.renderForm=this.renderForm.bind(this); //should be written outside state and inside constructor
    this.renderItem=this.renderItem.bind(this);
    this.toggleState=this.toggleState.bind(this);
        this.state={
   
        isEditing:false 
    }

    
}

toggleState(){
//In toggle fxn basically we need to setState ie. we need to permanent the state from toggle yes to toggle no ie. is Editing yes to no and vice versa.
this.setState({

    isEditing:!this.state.isEditing // or we can write like -> const isEditing=this.state.isEditing outside setState({}) and isEditing:!isEditing inside setState()
})
}

renderForm(){
    return(
<form> 
            <input type="text" defaultValue={this.props.detail.name}/> 
            <button type="submit">Update Item</button>
        </form>
           ) 
}

renderItem()
{  return(
<li onClick={()=>{this.props.clickHandler(this.props.index);} } className={this.props.detail.completed ?'complete':''}>{ this.props.detail.name}
        <button onClick={(evt)=>{evt.stopPropagation(); this.props.clickDeleter(this.props.index)}}>Delete</button> 
        <button onClick={(evt)=>{evt.stopPropagation(); this.toggleState()}}>Edit Item </button> 
        </li>
)  //By writing <button>Edit Item </button> we have created a button-Edit Item next to delete button clicking on which isEditing will become true/false ie. state will be changed and it will toggle to input textbox. So we would also need to create a toggle function for toggling isEditing and call it inside Edit Item button.
}

render(){

 const isEditing=this.state.isEditing; 
 
 
    return(

        <section>
         
        {/*convert list and input textbox fxnlty in a function-renderForm() and renderItem() and call them here.So based on isEditing either renderForm() will be called or renderItem() will be called*/}  
        { 
            isEditing ? this.renderForm(): this.renderItem()
        }
        </section>

    
)
}

}


export default TODOItem;
/* Till here we have seen edit fxnlty like: when we click on edit item next to delete button it 
changes list item into textbox inside which same list value is present and also that textbox is 
followed by Update Item button. Next we will see on changing the item and clicking on update Item
button change should reflect back to list item*/