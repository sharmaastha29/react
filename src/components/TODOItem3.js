import React from 'react';

class TODOItem extends React.Component{
    constructor(props){ // we are passing props here bcs it is child component and so it might require props.
        super(props);
        this.state={
   
        isEditing:false //To toggle btwn list and input textbox
    }
}

render(){
// In order to access isEditing inside render method either we need to do this.state.isEditing or we would need to crate a variable/constatnt and mk it hold this.state.isEditing in it. We can use without making varible also but everytime instead of using full this.state.isEditing, it is better to use var name.
 const isEditing=this.state.isEditing; //Now based on this isEditing var we need to toggle list and inputtext box.
 //const { isEditing }=this.state; and const isEditing=this.state.isEditing; both means same.
    return(

        <section>
         {/* Here, inside section tag we will use ternary operator concept ie: if isEditing true then input form should render else list. Since isEditing is js code so it should be within{}. This code will work fine but is little unorganized- we need to put list and form fxnlty inside seperate fxns and call them later instead -see in TODOItem4 .js*/}
          
        { isEditing ?
        <form> 
            <input type="text" defaultValue={this.props.detail.name}/> 
            <button type="submit">Update Item</button>
        </form>:
        
        <li onClick={()=>{this.props.clickHandler(this.props.index);} } className={this.props.detail.completed ?'complete':''}>{ this.props.detail.name}
        <button onClick={(evt)=>{evt.stopPropagation(); this.props.clickDeleter(this.props.index)}}>Delete</button> 
        </li>
         
        }
        </section>

    
)
}

}


export default TODOItem;

   