import React from 'react';
import PropTypes from 'prop-types'; //download npm PropTypes and import it here and when using it just write PropTypes.func etc.
// Alternatively, import * as t from 'prop-types';


class TODOItem extends React.Component{
    constructor(props){ 
        super(props);
    this.renderForm=this.renderForm.bind(this); 
    this.renderItem=this.renderItem.bind(this);
    this.toggleState=this.toggleState.bind(this);
    this.updateItem=this.updateItem.bind(this);
        this.state={
          isEditing:false 
                   }   
                    }


updateItem(evt){
    evt.preventDefault();       
    console.log(this.input.value); 
    this.props.clickediter(this.props.index, this.input.value);
    this.toggleState();   
}



toggleState(){ 
return(
this.setState({
      isEditing:!this.state.isEditing 
}))
}



renderForm(){ 
    return( 
<form onSubmit={this.updateItem}> 
            <input type="text" ref={(val)=>{this.input=val}}  defaultValue={this.props.detail.name}/> 
            <button  type="submit">Update Item</button> 
</form>
           )  
}



renderItem()
{  return(
<li onClick={()=>{this.props.clickHandler(this.props.index);} } className={this.props.detail.completed ?'complete':''}>{ this.props.detail.name}
        <button onClick={(evt)=>{evt.stopPropagation(); this.props.clickDeleter(this.props.index)}}>Delete</button> 
        <button onClick={(evt)=>{evt.stopPropagation(); this.toggleState()}}>Edit Item </button> 
        </li>
)  
}

render(){

 const isEditing=this.state.isEditing; 
    return(
        <section>
        { 
            isEditing ? this.renderForm(): this.renderItem()
        }
        </section>  
)
}

}

/* Adding propTypes-> for this we need to first install npm propTypes library into REACT16 -> import PropTypes from 'prop-types'; or yarn add prop-types AND 
IMPORT the library above . check link: https://www.npmjs.com/package/prop-types-> 
Current component name .propTypes{propname: React.PropTypes.datatype,....} 
So by doing this we restrict type of props*/
/*Also to make a prop compulsary/required we would need to make that particualr prop required--> clickHandler: PropTypes.func.isRequired So, if this clickhandler prop is removed/ not included then that particular react wil gv warning and component will not work as the component which is mentioned required is not included */
TODOItem.propTypes={
    // Alternatively, clickHandler: t.func.isRequired,
    clickHandler: PropTypes.func.isRequired,
    clickDeleter: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired,
    detail:PropTypes.object.isRequired,
    clickediter: PropTypes.func.isRequired

}

export default TODOItem;
