import React from 'react';
import PropTypes from 'prop-types'; 
import bindFunctions from '../util.js';//..to reach that file

class TODOItem extends React.Component{
    constructor(props){ 
        super(props);
    // this.renderForm=this.renderForm.bind(this); 
    // this.renderItem=this.renderItem.bind(this);
    // this.toggleState=this.toggleState.bind(this);
    // this.updateItem=this.updateItem.bind(this);
    bindFunctions.call(this, ['renderForm','renderItem','toggleState','updateItem'])

        this.state={
          isEditing:false 
                   }   
                    }


updateItem(evt){
    evt.preventDefault();       
    console.log(this.input.value); 
    this.props.clickediter(this.props.index, this.input.value);
    this.toggleState();   
}



toggleState(){ 
return(
this.setState({
      isEditing:!this.state.isEditing 
}))
}



renderForm(){ 
    return( 
<form onSubmit={this.updateItem}> 
            <input type="text" ref={(val)=>{this.input=val}}  defaultValue={this.props.detail.name}/> 
            <button  type="submit">Update Item</button> 
</form>
           )  
}



renderItem()
{  return(
<li onClick={()=>{this.props.clickHandler(this.props.index);} } className={this.props.detail.completed ?'complete':''}>{ this.props.detail.name}
        <button onClick={(evt)=>{evt.stopPropagation(); this.props.clickDeleter(this.props.index)}}>Delete</button> 
        <button onClick={(evt)=>{evt.stopPropagation(); this.toggleState()}}>Edit Item </button> 
        </li>
)  
}

render(){

 const isEditing=this.state.isEditing; 
    return(
        <section>
        { 
            isEditing ? this.renderForm(): this.renderItem()
        }
        </section>  
)
}

}


TODOItem.propTypes={
    clickHandler: PropTypes.func.isRequired,
    clickDeleter: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired,
    detail:PropTypes.object.isRequired,
    clickediter: PropTypes.func.isRequired

}

export default TODOItem;
