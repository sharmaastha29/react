import React from 'react';

/*We will edit the list items - for that we need to have state in the TODOItem component  so we 
would need to create TODOItem as a class based component instead of stateless functional component. We can do that by writing class TODOItem extends React.Component{

    constructor(){
     super();
     this.state={

     }
    }

    render(){   
    Also, by adding this.props infront of props    */
//const TODOItem = (props)=>{

class TODOItem extends React.Component{

    /*For incorporating the edit button fuctionality- there will be edit button next to delete 
    button for every list item. As soon as any of the edit button is clicked the corrs. list data 
    should come in a input textbox and there should be an update button next to the textbox so that 
    it can be modified. and after that data can be modified and once it is changed we need to click 
    on update buuton and it should become list format again with the updated value. So We need to 
    toggle between list and form bcs we need to convert from list to input textbox and then back 
    to list. For this we will create a constructor and create state in it. We need to create state 
    in constructor of TODOItem bcs from list item we want to toggle to input text box  */
    constructor(props){ // we are passing props here bcs it is child component and so it might require props.
     super(props);
     this.state={

     isEditing:false //we need to make isEditing false bcs based on this only it will decide when to display list format and when to display as an input textbox format
     
    }
    }

    render(){
    /*Now here what to render so for this we will create a section tag and we will comment the li 
    code. To comment the li code in JSX we need to create {}and  opening->/*  closing-> */
    return (
    <section>
        {/*We will create a form in which we will create input textbox and update button bcs we want
         to convert from list to input box and update button when we click on the button. We have given defaultvalue in those textboxes whichever was going to the list */   } 
        <form> 
            <input type="text" defaultValue={this.props.detail.name}/> 
            <button type="submit">Update Item</button>
        </form>

         {/*<li onClick={()=>{this.props.clickHandler(this.props.index);} } className={this.props.detail.completed ?'complete':''}>{ this.props.detail.name}
        <button onClick={(evt)=>{evt.stopPropagation(); this.props.clickDeleter(this.props.index)}}>Delete</button> 
        </li>*/}
         
         {/*Now, we need to decide whether we need to render list or input textbox depending upon isEditing: true or false. So, we need to toggle between list and input textbox. For that see TODOItem.js*/}
         
    </section>

    
    )
    }

}


export default TODOItem;