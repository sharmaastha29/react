/* Creating a new Component */
import React from 'react';
import PropTypes from 'prop-types';

const TODOForm=(props)=>{

return(
            
<form onSubmit={props.clickSubmitter}>
    <input type= "text" value={props.detail1} onChange={props.clickUpdater}/>
    <button type="submit" >Submit</button> 
</form>

)

}

TODOForm.propTypes={
    detail1: PropTypes.string.isRequired,
    clickUpdater:PropTypes.func.isRequired ,
    clickSubmitter:PropTypes.func.isRequired

}

export default TODOForm;

/*onChange event tells that as soon as the textbox value is changed the corresponding function will trigger */
/* onSubmit event tells as soon as the button is clicked or enter key is pressed the corresponding
function will trigger. Also, we have mentioned here button type=Submit -> it helps in clicking on
submit button and as soon as it is clicked corrs. function is called*/

/* events are always put beside the tag,; inside it--> onSubmit={props.clickSubmitter}=>on hitting
 enter as well as on clicking submit button; both ways  it will trigger corrs. function. Also, <button type="submit" >Submit</button> tells on clicking button corrs. function is triggered. So in this only one thing is happening */