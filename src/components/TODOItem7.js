import React from 'react';
//import React, {Component} from 'react'; // if we do this then we can directly write class TODOItem extends Component instead of React.Component.

class TODOItem extends React.Component{

//If we make Addtodo mtd here then we would need to push new todos in state but here we can't get those state so we would need to make this mtd in index.js

componentWillReceiveProps(nextProps){ //here nextProps is is the arg which componentWillReceiveProps() mtd takes and it is the arg which works when the mtd gets nextProps ie. when the props is changed ie. for second time when props comes.
    console.log("new array  "+nextProps.todo); // for next props-> expected result: [i am astha, i am sharma, i am as]; actual result: [i am astha, i am sharma, i am as]
    console.log("old array  "+this.props.todo); // for current props-> expected result: [i am astha, i am sharma]; actual result: [i am astha, i am sharma, i am as]

}



/*   Add(){
    console.log(this.input.value);
    this.props.clickAddTODO( this.input.value);


}  */ 


    render(){ // this TODOItem component will be rendered as many times as the no. of items are there in the array so if we create AddTODO button in this it will be rendered lots of time so create it in index.js or as a separate component.

        return(<section>{
            /*<form onSubmit={this.Add}>
            <input type="text" ref={(val)=>{this.input=val}} />
            <button type="submit">Add Todos</button>
        </form> */

            /*<li>{this.props.det1}</li>*/
            this.props.todo.map((tdo, index)=>{ return <li key={index}>{tdo}</li> })
         } </section>
    )
}
}

export default TODOItem;

/*We can make input textbox and button as a separate component or merge within list component. 
Here, we are merging it within the same list component. */