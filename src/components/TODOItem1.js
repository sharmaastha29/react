import React from 'react';


//WAY1- To create separate folder for Component and remain it as Class Based Component
// class TODOItem extends React.Component{

//     render(){
 
//           return (<li onClick={()=>{this.props.clickHandler(this.props.index);} } className={this.props.detail.completed ?'complete':''}>{ this.props.detail.name}</li>)
//           /*className={this.props.detail.completed ?'complete':''} is for setting css properties */
//         }
// }


//WAY2- Converting TodoItem from class component to stateless functional component as it doesn't have any state in it.
 const TODOItem = (props)=>{
 return (<li onClick={()=>{props.clickHandler(props.index);} } className={props.detail.completed ?'complete':''}>{ props.detail.name}
 <button onClick={(evt)=>{evt.stopPropagation(); props.clickDeleter(props.index)}}>Delete</button> 
 </li>)
 /*Here, we need to pass event inside onclick fun bcs using that event we need to stop propogation 
 of clicking to parent class components. here, what is happening is when we are clicking delete 
 button automatically the event will be propogated and looped to call the li tag also and so on 
 its parent so to stop looping/propogating we need to do evt.stop propogation inside onclick of 
 button */

 /*Inside li tag we have made a button tag which will add button called delete infront of each 
 individual item and on this button when it is clicked it will call corrs. function and how to know 
 which button is clicked ie: for which item we will use index ..so we will pass index inside 
 clickDeleter function just as we did for clickHandler function; index is already created once */
/*  As this keyword will not work in this we need to pass props as arg of this stateless fxn*/
}


export default TODOItem;

/* If there is a component which is having state inside it, it is mandatory for that component to be
 a class component like: TOdoList component but for components which donot have any state it is not 
 required for them to be class component i.e: class TODOItem extends React.Component, we can change it to stateless functional component as 
 there is no state inside it. We can see how that is done. For doing so we just create a fxn with 
 const keyword which will return some JSX. But in stateless component value of this keyword is not 
 valid then how to assign props if this keyword is not working. For that we would need to pass props 
 directly as a parameter inside the stateless function. This is different from custom fxns bcs it 
 starts with const keyword.
 
 Also, IMP point to remember is the file name and the component should be same else no-def error will occur.*/