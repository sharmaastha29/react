import React from 'react';

class TODOItem extends React.Component{
    constructor(props){ 
        super(props);
    this.renderForm=this.renderForm.bind(this); 
    this.renderItem=this.renderItem.bind(this);
    this.toggleState=this.toggleState.bind(this);
    this.updateItem=this.updateItem.bind(this);
        this.state={
   
        isEditing:false //It lets toggle state from list to input textbox  depending upon it is true/false
    }

    
}
// This is how mtd is called from parent and used in child. Also this.input can be used globally.
updateItem(evt){
    evt.preventDefault();
   
        
console.log(this.input.value); // So as soon as update Item button is clicked updateItem() triggers which will give this.input.ie. textboxs' value on console. This way task 1 is completed. Now we need to do 2 task of reflecting that item in list. For that we would need to pass data from child to parent so that it can add that data to tasks array and again display on screen via TODOItem component. 
   //Here, we want that this input value should be hold as soon as we click on update Item button and should give it to another fxn so that it can display that value as list. For displaying in list we need to add this new data to tasks array and so for that we need to make a fxn in index.js which will add that data to list and we need to send that fxn to TODOItem child component and use it inside updateItem fxn right after we get the input value.
    this.props.clickediter(this.props.index, this.input.value);
    this.toggleState(); // so that after new value from textbox is added again on update button click it should show as a list with updated item.
   
   
}



toggleState(){ //It tells on clicking edit item the list will bcm textbox and vice versa
return(
this.setState({

    isEditing:!this.state.isEditing 
}))
}

renderForm(){ //It renders the inputtextbox and update button
    return( 
    /* Here, in nextlines we have created button type submit as well as onSubmit event on form bcs 
    if someone hits enter or clicks the update button it should work.Inside onSubmit we need to call 
    a function called updateItem which will update the list item with the new one which is there in 
    the textbox. We can call it using this.updateItem bcs it is defined in the same componenet.Here 
    we are using ref to hold the input coming from textbox and so that it can be used again 
    anywhere in the component. here, val is any random variable used.It will take input from 
    textbox and store in val variable which is later on stored in this.input so that it can be used 
    anywhere just like a variable. this is how we hold data from textbox. So, if we want to use textboxs' value we can simply take this.input.value*/
<form onSubmit={this.updateItem}> 
            <input type="text" ref={(val)=>{this.input=val}}  defaultValue={this.props.detail.name}/> 
            <button  type="submit">Update Item</button> 
</form>
           )  
}

renderItem()// It renders list and delete button and edit button- edit button toggles state from list to textbox depending upon isEditing true/false and reverses the state by calling toggleState().
{  return(
<li onClick={()=>{this.props.clickHandler(this.props.index);} } className={this.props.detail.completed ?'complete':''}>{ this.props.detail.name}
        <button onClick={(evt)=>{evt.stopPropagation(); this.props.clickDeleter(this.props.index)}}>Delete</button> 
        <button onClick={(evt)=>{evt.stopPropagation(); this.toggleState()}}>Edit Item </button> 
        </li>
)  
}

render(){

 const isEditing=this.state.isEditing; 
 
 
    return(

        <section>
         
        
        { 
            isEditing ? this.renderForm(): this.renderItem()
        }
        </section>

    
)
}

}


export default TODOItem;
/*Here, we will look into update functinality. When we should be able to edit textbox and on 
submiting(onsubmitting and not on click bcs by submitting we can hit enter as well as click on 
button and it will work for both) on form tag of update Item button it should update and reflect into list format.*/

/*We need to perform two things- 1. hold value from textbox, 2. on clicking update it should update new item to list  */