/*Lifecycle method of React- componentWillReceiveProps()- This is another lifecycle method in react.
It is not recommended to use this method. It is not called initially. It is called only when props 
changes. It is not recommended to use this method bcs it reflects same values of array even if we 
add some new element to it.  It shows updated array for both old and new arrays since arrays are 
pass by reference. which means if we make changes to new array, it will also change the old array 
so mutability problem  comes. So, componentWillReceiveProps() mtd will lead to mutability. Thus we 
need to make it immutable. To make arrays immutable we can use concat /slice etc ways. By concating 
new value to old array a separate new copy of array is made with updated array and it will not modify
the existing array rather it will create an entirely new array. Please see below code to understand 
more. 
foo: [1,2,3];
bar:foo;//assigning foo array to bar so bar becomes foo now-->[1,2,3] ie: pass by reference happened here.
bar.push(4);
bar;--> [1,2,3,4]// value of bar becomes 1,2,3,4 as expected since we are pushing 4 to array bar
foo;-->However if we print foo we will see the value of foo is also modified and it also became 1,2,3,4 
like bar. This is bcs array is pass by reference so values will mutate and  any changes to one array 
can lead to changes in old array.
To solve this mutable probem and to mk it immutable ppl in proffesional env uses immutablejs.Here,
we are using concat fxn instead of push on array in TODOAdd() to achieve the same. concat() when put on array todo will return a new array 
with exsisting array elements plus new elements. So, old array initially will have its elements and 
new array will have old array's plus concatinated value. In this way immutability is achieved. We 
can also do same by slice() and immutablejs lib.

*/

import React from'react';
import ReactDOM from 'react-dom';
import TODOItem from './components/TODOItem.js';

class TODOS extends React.Component{
constructor(){
    super();
    this.AddTODO=this.AddTODO.bind(this);
    this.state={
        todo:["i am astha", "I am sharma"]
    }
}



AddTODO(evt, val){ // val from this.input can't be accessed here so we need to mk half of this fxn in TODOItem and call other half of the fxn  from index.js
   // let td=this.state.todo;
    
        evt.preventDefault();
        console.log(this.input.value);
        let td=this.state.todo;
        //let currVal=this.input.value; 
        //td.push(this.input.value)// leads to mutability
        let t=td.concat(this.input.value)//provides immutability as new copy of array will be created on using concat fxn and new value is added to new copy of array and not in the old array.
        this.setState({
        // todo:td.concat(this.input.value), //do concat inside setState as it will modify here or else save it in a variable and pass here inside setState fxn.
        todo:t
        })
        this.input.value='';
        
    //let t=td[index];// required for update to fetch that particular record and not needed for add fxnalty
    
}



render(){
    return(
        <section>

        <form onSubmit={this. /*Add*/ AddTODO}>
            <input type="text" ref={(val)=>{this.input=val}} />
            <button type="submit">Add Todos</button>
        </form>
         <ul>  
        {/*{this.state.todo.map((tdo, index)=>{return(<TODOItem todo={this.state.todo} det1={tdo} key={index} clickAddTODO={this.AddTODO}/>)})}</ul>*/}
         <TODOItem todo={this.state.todo} /*det1={tdo}*/ /*key={index}*/ clickAddTODO={this.AddTODO}/>
         </ul>
        </section>
        
    )
}

}

ReactDOM.render(<TODOS/>, document.getElementById('root'))                                      