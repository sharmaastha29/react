/*Here, we will see - Children design pattern and how to reuse components in react 

/*  here, we will create an App component which wll render section 
tag. Also, there will be one more component which will have ajax 
call to a url in its componentDidMount(). It will set the data coming 
from that url to child component's content variable in state.Here, 
we will see how child's state is called in parent */
import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import PropTypes from 'prop-types';


class App extends React.Component {
    //As no state is required in parent so no constructor.//there can be any no. of such components giving different urls. But only one reusable component which will be reusing any kind of url.

    render() {
        return (
            //------------------------------------In parent NEW CODE FOR CHILDREN DESIGN PATTERN------------------------------------------------
            <section>
                <Fetch url="https://jsonplaceholder.typicode.com/posts">
                    {/* Now in order to pass content state to parent we will use children design pattern-> which will be done in form of fxn */}
                    {(abc) => { // abc is a variable taken by this fxn which will receive data from child and gv it here to parent
                        return abc.map((dta, index) => { //Here, abc holds the array content and dta var holds one array element at a time and returns that array element's title in list format.
                            return <li key={index}>{dta.title} </li>
                        })
                    }
                    }
                </Fetch>

                {/*--------------------------------For another url  eg:inside same section tag as render() returns only one tag -------------------------------------------*/}
                User
            <Fetch url="https://jsonplaceholder.typicode.com/users">
                    {(abc) => {
                        return abc.map((dta, index) => {
                            return <li key={index}>{dta.name} </li>
                        })
                    }
                    }
                </Fetch>

            </section>


            //------------------------------------------------------------------------------------------
        )


    }
}




// Here, Fetch is  a child component which contains ajax calls. It is a reusable component and can be called in multiple classes to make ajax calls to different different urls. This component will use children render design pattern and we will see how child is being called in parent component.
class Fetch extends React.Component {
    constructor()// constructor bcs we want to have state in this component 
    {
        super();
        this.state = {
        content: []// content is an array which will contain the data coming from ajax call. Remember ajax requests are done inside ComponentDidMount().
        }

    }

    componentDidMount() { //componentDidMount is used for making ajax calls
        $.ajax({ //we are using utility mtd ajax here with the help of '$' symbol.
            url: this.props.url,//since this Fetch component is reusable so we can't gv any static url here. Url will keep on changing so we need to give dynamic url here with the help of this.props.url; url in this.props.url is coming from root parent component.
            success: (data) => { // a success mtd which will receive data coming from url in 'data' variable  and we will set it to content by using setState().
                this.setState(
                    {
                        content: data
                    }
                )
            },

            error: (e) => { // an error mtd to give error if any happens
                console.log("err", e) // e is the error object coming and err is the label
            }

        })
    }

    render() {
        return (
            //-------------------------------In Child NEW CODE FOR CHILDREN DESIGN PATTERN--------------------------------------------------------------
            <section>
                {this.props.children(this.state.content)} { /*Here, we are using children api syntax and with help of that we are passing state content so that it can be used in parent */}
            </section>
        )

        //-------------------------------------------------------------------------------------------------

    }

}
// Now, the main challenge is how we will pass this content which has all the data coming from url 
// to parent component App bcs we want to render it inside section tag of App component. ie:
// how to pass data from child to parent. We do this by Children render design pattern  
// PROCEDURE:- 
// 1.We will call <Fetch/> component inside section tag of App component like we always do but it shld not be self enclosing tag; rather it shld be opening and closing tag.
// 2. Inside this opening and closing Fetch component tag we will write code of Children api design pattern-> we will give url attribute to Fetch component as we always do normally when we send parent data to child.
// So, to call child component's data in parent we simply need to call child component(opening n closing one! not self enclosing one!) in parent and then we give the property attribute used in child here: url
// 3. Make Children design pattern fxn and loop array via map.
// so, in nutshell, inside child component who is mking dynamic ajax calls we will simply gv this.props.children(this.state.varnm) inside render(). And in parent who is gvng diffrnt urls we simply call Child component and pass some url as attribute and loop over that child components state varnm in a fxn inside render mtd. 
// We can use our detch component as many times as we want inside App component for different url or outside in different component for different url.


/*As in fetch component url is required so in Proptypes we will write that variable as required */


Fetch.propTypes = {
    url: PropTypes.string.isRequired
}


ReactDOM.render(<App />, document.getElementById('root'))   
