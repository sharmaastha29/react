import React from 'react';
import ReactDOM from 'react-dom';

class TODOList extends React.Component{

    constructor(){
    super();   
    this.changeStatus=this.changeStatus.bind(this);//so that changeStatus mtd can access this.state
    this.state={
        tasks:[
        {name: "Buy Milk",
        completed:false},

        {name:"Buy Cheese",
        completed: false},

        {name:"Buy Bread",
        completed: false}
              ]
                }
                  }
      
      changeStatus(idx){
      /* EventHandlerFlow-2.In order to access this.state in custom method we need to 
      bind it-> see in constructor before state is mentioned*/
      console.log(this.state.tasks[idx]);//console.log shows logs on console. index we are passing in changeStatus fxn bcs we want to access tasks array inside this fxn and since tasks is array we would require index to access its elements*/
      
      }  
    



      render(){
          return (<ul>{this.state.tasks.map(/*function*/(task,idx)=>{
                        return <TODOItem clickHandler={this.changeStatus} index={idx}  key={task.name} detail={task}/>
        /* In order to pass any info (all info is stored in state) 
        from parent to child we do that by using properties(properties->
        props are nothing but variable); i.e: we 
        call child class component in return.
        pass all info of state  via a variable. Also, similarly if we 
        want to pass any custom function in child we need to do so via 
        variable. Same is for unique key...In short if we need to pass 
        anything from parent to child component we do that via 
        variable. Whatever properties/variables we are mentioning 
        when we are calling child component in parent only those 
        can be used in child compnent*/

        /* EventHandlerFlow-3. By doing clickHandler={this.state.changeStatus} we are 
        passing custom fxn to child component via a variable. 
        Actually in this.state.changeStatus this.state won't work bcs 
        the anonymous fxn which we have created is custom fxn and it 
        changes value of this. So we need to use fat arrow fxn instead 
        of anonymous fxn*/

        /*Put index in index variable as it is required for the fxn 
        chageStatus and we are passing changeStatus to child so 
        index must also be passed as the fxn changeStatus should 
        accept index */

          })}</ul>)
      }   
      
      /* Everything in child is accessed by props and 
      everything of parent is accessed by state. this.state and this.props*/ 
      


}


class TODOItem extends React.Component{

render(){
    return (<li onClick={()=>{this.props.clickHandler(this.props.index);}} className={this.props.detail.completed?'completed':''}>{this.props.detail.name}</li> )
    /*In index1.js we did directly this.props.detail bcs detail was
     variable holding array of single strings but here detail is 
     holding object i.e: key, value format so you have specify which
     key we want to display. Also, if we do this.props.detail.completed
     it will show empty bullets bcs boolean values aren't displayed */

     /* EventHandlerFlow-1.className={this.props.detail.completed?'complete':'' is used to 
     check the condition -->if this condition is true then only on 
     clicking li tag state should change */

     /*EventHandlerFlow-4.  onClick={this.props.clickHandler}--> use 
     the fxn in Child inside onClick event.So, as soon as li tag is 
     clicked clickHandler var which holds changeStatus fxn is 
     triggered and respective action takes place. But, since map 
     fxn is custom so this.state won't work there for clickHandler 
     so in child when we call inside onClick it gives error so it is 
     replaced by fat arrow fxn. Also, pass index in child component call bcs it is taken as a parameter by changeStatus fxn*/
}

}

ReactDOM.render(<TODOList/>, document.getElementById('root'))

/*To change data dynamically on click of ay event. We need to go to 
that event (here, when we click on li it should show name of item if 
status  completed is true). It means event is on clicking li so our 
condition will be put on li tag and data which needs to be changed 
is of parent component's state.That data we will toggle/change by 
creating a customised function and then passing that function in a 
variable wherever we are calling the child component in parent 
component. Also, when we create any custom function we can't pass 
this.state diractly bcs it will take. All inbuilt functions can take 
this.state but not cutom functions bcs inside custom fnxs value of this changes so for making custom functions 
take this.state we need to mention it in constructor just above 
state--> this.changedStatus=this.changedStatus.bind(this) . 
Alternatively, we can go for fat arrow functions ()=> for the same.
To pass this function to child component inside parent component we 
need to store it in a variable whereevr  we are calling child 
component and we can now access that function in child component 
using props inside onclick of li tag(tag on clicking which we want 
some action/function to occur )
*/

/* render method is inbuilt and not the customised method and it 
always returns one tag which is internally converted to a function JSX-Java Script Extension*/