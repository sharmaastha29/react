/*SetState()  mtd in detail->asynchronous (immediately after setState , the state maynot reflect the changes)and synchronous states(immediately after the state change the changes will be reflected )
Usually setState gvs async calls but if we want sync calls then we can add a fxn parameter to setstate mtd or directly do fxn call and pass a parameter called prevState which holds prev state in case some other mtd modifies the this.state.varnm.
*/
import React from 'react';
import ReactDOM from 'react-dom';

class Counter extends React.Component{
    constructor(){
        super();
        this.incCount=this.incCount.bind(this);
        this.state={
            count:0
        }
    }
//Originally-Asynchronous-In Console if inc is 1, it shws 0 and so on.
    // incCount(){
    // this.setState({
    // count:this.state.count+1
    // });
    // console.log(this.state.count)
    // }

// Synchronous- WAY1-In console if inc is 1 it shows 1 and so on.
    // incCount(){
    // this.setState({
    // count:this.state.count+1
    // }, ()=>{
    //  console.log(this.state.count)
    // });
    // }
    
//Synchronous- WAY2-setState not only returns object but fxn also and we use this technique so that 
//we get original value and not the value which might be modified by some other mtd that uses 
//this.state.count.
    incCount(){
    this.setState((prevState)=>{
     return{count:prevState.count+1} //here prevState is inbuilt parameter which will hold the prev state is the copy of original state so we don't have to worry in case some mtd modifies the original state as we have the copy of the same in prevState - the one which is unmodified by any other fxn -the original state.
     
    }, ()=>console.log(this.state.count));
    }

    //Now, this is a simple incCount fxn which increments the value of count when add number button is clicked.
    //In abv incCount mtd, if we put console.log(this.state.count) right after setState mtd is 
    //completed, it shld show modified value as per the expectation but no! it will not show 
    //updated value.The reason behind this is that setStae mtd is Async in nature and so the value 
    //modified in it might take longer time to get reflected and not immediately. We can verify this 
    //if we see console section by pressing F12. There if inc Count is 1 it will show 0 in console. 
    //If inc 2 then it will show 1 in console and so on. However, if we 
    //want to mk same setstate mtd as sync mtd then after setting state we need to call a fxn as a 
    //parameter of setState mtd which will reflect the same value immediately after setState completes.
    //Also, there is an alternative as well for maintaining the state. this.state.count can be used 
    //by 2 mtds at the same time and there are chances that their value gets modified and other mtd 
    //gets modified value of count instead of original value of count. This leads to inconsistent 
    //data. To avoid this, inside setstate mtd we use prevState parameter which holds  copy of 
    //prevState and thus even if the this.state.count value is modified by some mtd it won't affect 
    //the other mtd.

    render(){
        return(
            <section>
                {this.state.count}
                <button onClick={this.incCount}>Increment Number</button>
            </section>
        )
    }
}

ReactDOM.render(<Counter/>, document.getElementById('root'))