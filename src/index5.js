/*Adding propTypes in React- PropTypes are used to specify the type of props strictly. 
Suppose we made props called name  in parent and we use it in child and by mistake we made made as 
function or something else in parent so by specifying propsTypes in child we can restrict/strictly 
control its Type as string(say eg). So we made the propType strictly for string and not anyother 
type of data can come into it now else react will give warning when compiled. We can't use propType
for parent component- index.js as parent only passes props but never uses it. We can use  propType only 
child components- TODOItem, TODOForm. First for TODOItem we will see -> In index.js if we see out of all the props which we are passing apart from key for all others we can specify propsType in TODOItem.js. For key we aren't specifying bcs it is a react property and not some prop created by us.
Similarly, for TODOForm, we can specify propType for all the props in TODOForm.js.
Now, exactly where we need to specify propsType for props in TODOItem, TODOForm are either ainside the component class above constructor or above export line after the component class ends. See TODOItem, TODOForm for details.*/


import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TODOItem from './components/TODOItem.js'; 
import TODOForm from './components/TODOForm.js';


class TODOList extends React.Component{

constructor(){
super();
this.changeStatus=this.changeStatus.bind(this);
this.updateTask=this.updateTask.bind(this);
this.addTask=this.addTask.bind(this);
this.deleteTask=this.deleteTask.bind(this);
this.editItem=this.editItem.bind(this);
this.state={
tasks:[
    {name:"Buy Milk",
    completed:false},

    {name:"Buy Cheese",
    completed: false},

    {name:"Buy Bread",
     completed: false}
      ],
currentTask: ' '      
}
}



editItem(index, newVal){
console.log(index, newVal);
let tsk=this.state.tasks;
let t=tsk[index]; 
t['name']=newVal;
this.setState({
    tasks:tsk
})
}





deleteTask(index){ 
let tsk=this.state.tasks;
tsk.splice(index, 1);
this.setState({     
tasks:tsk
})
}


addTask(evt){
evt.preventDefault();
let tsk=this.state.tasks; 
let currTask=this.state.currentTask; 
tsk.push({ 
    name:currTask, 
    completed:false 
})
this.setState({
tasks:tsk,
currentTask: ' ' 
})
}


updateTask(newValue){
this.setState({currentTask:newValue.target.value})
}


changeStatus(idx){
console.log(this.state.tasks[idx]) ;
var tsk=  this.state.tasks; 
var t=tsk[idx]; 
t.completed=!t.completed; 
this.setState({tasks:tsk}) 
}


render(){ 
    return( <section> 
            <TODOForm  detail1={this.state.currentTask}/*currentTask is in state so we will call it using this.state.currentTask */ clickUpdater={this.updateTask} clickSubmitter={this.addTask} />
            <ul>{this.state.tasks.map((task,idx)=>{
            return <TODOItem clickHandler={this.changeStatus}  clickDeleter={this.deleteTask} index={idx}key={/*task.name*/idx} detail={task} clickediter={this.editItem}/>
   // Here, if we change index={""+idx} it will bcm string type instead of number and so react will give error saying invalid prop 'index' of type 'string' supplied to TODOItem, expected 'number'.
   })}
    </ul>
    </section>)
}
}




ReactDOM.render(<TODOList/>, document.getElementById('root'))
