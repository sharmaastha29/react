import React from 'react';
import ReactDOM from'react-dom';
import './index.css';
import $ from 'jquery'; // install jquery for npm -> https://www.npmjs.com/package/jquery

/*LifeCycle Methods of React.js in order:
1. constructor()
2. componentWillMount()
3. render() 
4. componentDidMount()// for ajax calls etc
5. shouldComponentUpdate()-> it takes a boolean value. If this boolean value is true then component
will rerender else it won't re render. Also, initially when the component is rendered for the first
time it is not invoked. but if component is rendered again it is called and boolean value is checked. If it is true it re renders component else not.
Out of these constructor, componentWillMount, componentDidMount are called only once but render 
will get called as many number of times as the component is modified. It means react rerenders only 
the component which is changed again and again and not refreshes the entire page bcs if entire page 
would have refreshed or rerendered then constructor, componentwillmount, componentDidMount all would 
have re rendered but it is not happening here as only once they are printed in console.log.
-> shouldComponentUpdate doesn't gets called for the first time but it gets called when component is rendered again.
We will see this with the help of Increment count button which when gets clicked will increment the 
state/variable count. Always state is written in constructor as it is first thing to get called
6. ComponentWillReceiveProps()- not recommended to use- called/invoked only when props change- leads to 
mutability- so to avoid mutability and promote immutability we need to go for immutablejs/concat()/slice()
7. ComponentDidCatch()- error handling in react.
8. ComponentDidUpdate()- alt for ComponentWillReceiveProps
*/
class App extends React.Component{

constructor(props){
super(props);
console.log("constructor");

this.state={
    count:0 //initializes variable count to 0
}
}

/*normal function ; alternative for incCount(){
this.setState{

}
}*/


incCount=()=>{
    this.setState({
     count:this.state.count+1 //count=count+1; --> increments the value of count
    })
}


componentWillMount(){
console.log("will mount");
}


shouldComponentUpdate(){
if(this.state.count>10){
    return false; // if count is greater than 10 component will not re render ie: render mtd will not be called but if you check react dev tool then state would increment but in screen increment won't be there bcs re rendering will not happen.
}
else{
    return true; // if count is less than 10 component will be re rendered ie: render mtd will get called
}

}


render(){
    console.log("rendering");
return (<section>
{this.state.count}
<button onClick={this.incCount}> Increment</button>

</section>)
}


componentDidMount(){
    console.log("mounted"); //here ajax rquests are done
}


}


ReactDOM.render(<App/>,document.getElementById('root'))