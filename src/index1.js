import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


class Hello extends React.Component
{
render(){
return (
<h1>Hello!</h1>
)  
}
}



class World extends React.Component
{
    render(){
            return(
                <p>World!</p>
            )
        }
}



class HelloWorld extends React.Component{


constructor(){
super();
this.name="Sharma"
}


getName(){
 return "Astha"; 
}


render(){
/*js syntax to be enclosed within {}; only expressions allowed 
and not if, while etc loops.Also CSS is called using className 
instead of Class*/

    return <section className="foo">{this.getName()} { this.name} <Hello/><World/></section>
}
}



class TODOList extends React.Component{


    constructor(){
    super();
    this.state={
    firstname: 'AST',
    lastname:'SHAR',
    hobbies:['dance','sing','read']
               }
    }


render(){

    return ( 
    /*Since multiple lines in return so put () braces  */
    
    /* return can return only one tag bcs all tags when compiled 
    are converted to JSX and in JSX tags are converted to 
    createElements() and only one function can be returned from 
    return keyword. So, we need to wrap two tags in one outer tag 
    in order to return one tag*/

    /* prop is nothing but the property/attribute/variable in which you store state variable's values  to 
    be passed to child component which you have created; the same variable you have to
    pass in child component also so that child can access the parent component's state variables;
    It helps in passing state's value from parent component to child component. It passes variable name so in child that variable's value is rendered--> eg: we 
    provide extra css property/attribute called classname="foo" 
    to inbuilt   <div> tag */

    /* state is used to store some values in variable called state to render */
    <div><ul>{this.state.firstname}</ul>
    <ul>{this.state.lastname}</ul>
    <TODOItem details={this.state.firstname} details1= {this.state.lastname}/>
    {this.state.hobbies.map(function(hobb){

    /* return hobb -->returns all in single line-->dancesingread;
     to add style we need to provide property; map is foreach loop*/
    /*Whenever we are using array/iteration then so that react comes 
    to know which element of loop is rendering we need to use a unique key 
    which tells react which individual item itis currently holding */ 
     return <TODOItem key={hobb} details={hobb}/> 
    })}
    </div>)

    /*Here, property name details is customized name which we 
    have kept. And so that this detail property can be used  we 
    need to mention  props followed by property name in TODOItem 
    so that TODOItem knows it is extra property made for it 
    Also we can pass n number of properties*/
}

    
}

/*React DOM is required in order to render react components in screen */
/*React Flow- 1-ctrl will come to ReactDOM ; 
it will see which component
it will go to that component and do whatever is stated there  */

class TODOItem extends React.Component{
    render()
    {
        console.log('output',this.props.details);
        console.log('output1',this.props.details1);
        return <div><li>{this.props.details}</li>
        <h1>{this.props.details1}</h1>
       </div>
    }
}

ReactDOM.render(/*<HelloWorld/>*/  <TODOList/>,document.getElementById('root') )
  