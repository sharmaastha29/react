/*Here, we will see 1. Children API. Here, we will be covering 1. ptn only- Children API. 2. ptn - Children design pattern we will be cover in another index.js file. 2. It helps to create resuable 
components. Also, it helps us to understand how states/variables can 
be passed from child to parent. Also, it tells us about 3. Prop Design 
Pattern */

/*  here, we will create an App component which wll render section 
tag. Also, there will be one more component which will have ajax 
call to a url in its componentDidMount(). It will set the data coming 
from that url to child component's content variable in state.Here, 
we will see how child's state is called in parent */
import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component{
//As no state is required in parent so no constructor.

render(){
    return(
        <section> 
        {this.props.children}  {/*Since it is recieved as children so here we need to call it using props -> this.props.children */}
        {React.Children.only(this.props.children)} {/*React also gives some inbuilt fxns eg: React.children.only-> it displays only one children. As here, we are having more than one children and in render we have mentioned children.only which means only one child it will render but here we are writing so many children so it willn't display any:-> React.children.only is expected to receive a single React element child. If we wrap all the <p> tags into one <div> tag then it means we are passing only one child element and so on writing React.Children.only it will render that div which has multiple <p> children  */}
        </section>
    
    )
}
}
//There are several other react inbuilt fxns which they have given like: React.Children.count(), React.Children.toArray, foreach, map  etc.

//ReactDOM.render(<App/>, document.getElementById('root'))   
ReactDOM.render(<App>  
    <div>
    <p> Helooz</p>  {/* Since we have App opening and closing tag whatever written in btwn them is considered as Children to that App Component and to dilsplay it we need to specify where to render it inside render() mtd. */}
    <p> Helooz</p>  {/*We can mk multiple child also and render them just by writing this.props.children-> it will render all the mentioned children. We can also pass fxns etc in this */}
    <p> Helooz</p>
    <p> Helooz</p>
    </div>
</App>, document.getElementById('root'))  
//here, if we make <App/> tag from self enclosing to open and close tag separately and if we write anything in between those opening 'n' closing tags then it will be treated as child of that App component.  